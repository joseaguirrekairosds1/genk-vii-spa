FROM node:20.9.0-alpine3.18 as builder
RUN mkdir /my-app
WORKDIR /my-app
COPY . .
RUN npm ci
RUN npm run build -- --output-path=dist

FROM nginx:1.25.3-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /my-app/dist/browser /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]